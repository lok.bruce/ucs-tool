#!/usr/bin/env python
'''
This script tested to work on both C220M3 with CIMC 2.0(9f) and 3.0(3a)
It can configure a standalone C-series servers with one single script and Yaml file
Syntax: 
$ python config_cimc_yaml.py -i ip -u admin -p password -c True -b config_yaml/vnic2_m5_c220.yml
'''

# we use raumel.yaml instead of just yaml because it can read the yaml file and give it back to
#   us in the order in which its in the the file. This is important because have some settings
#   require objects to be created first before other operations can be performed.
from imcsdk.imchandle import ImcHandle
from imcsdk.mometa.adaptor.AdaptorHostEthIf import AdaptorHostEthIf
from imcsdk.mometa.lsboot.LsbootPxe import LsbootPxe
from imcsdk.mometa.lsboot.LsbootHdd import LsbootHdd
from imcsdk.mometa.lsboot.LsbootSd import LsbootSd
from imcsdk.mometa.lsboot.LsbootUsb import LsbootUsb
import argparse
import ruamel.yaml as yaml
import re
import time
from signal import *

def cimc_login_timeout_handler(signum, frame):
    raise Exception("CIMC login timeout")

# quick and dirty set whats in the dict. Don't check just set it.
def modifyMO(ucs, mo, desired_settings):
    for key, value in desired_settings.iteritems():
        try:
            setattr(mo, key, value)
        except ValueError as e:
            print "ValueError: %s"%(e)
            pass
    ucs.set_mo(mo)
    #print mo

def getParentDn(dn):
    p = re.compile('sys.*/')
    parentDnStr=p.findall(dn)[0]
    # remove the last '/'
    parentDn = parentDnStr[:-1]
    return parentDn

def createMO(ucs, classid, dn, desired_settings):
    parentDn=getParentDn(dn)
    compName=desired_settings['name']
    # delete name attribute from modify parameters
    del desired_settings['name']
    print "classid:%s, parentDn:%s, compName:%s"%(classid, parentDn, compName)

    # instantiate object with either eval(classid)() or globals()[classid]() from class name str
    #obj=eval(classid)(parentDn, compName, **desired_settings)
    obj=globals()[classid](parentDn, compName, **desired_settings)
    #print obj
    return obj

def modifyExistingMO(ucs, dn, desired_settings):
    obj = ucs.query_dn(dn)
    if not obj:
          print "Error: Object with Dn <%s> not found, please Check your yaml config against the CIMC object model"%(dn)
    else:
       print "Object with Dn <%s> found - modify"%(dn)
       print "-------------- modifying ---------------"
       modifyMO(ucs, obj, desired_settings)

def checkExistingMO(ucs, dn, csettings):
    obj = ucs.query_dn(dn)
    if not obj:
        print "Error: Object with Dn <%s> not found. Please check your yaml config against the CIMC object model" %(dn)
    else:
        curr_class = obj.__dict__
        print "Object with Dn %s found:" %(dn)
        for key, value in desired_settings.iteritems():
            curr_value = curr_class[key]
            print "[%s] current value: [%s] desired value: [%s]" %(key, curr_value, value)

# if MO with dn exist, modify, otherwise, create a new MO
def modifyOrCreateMO(ucs, classid, dn, desired_settings):
    obj = ucs.query_dn(dn)
    if not obj:
        print "Object with Dn <%s> does not exist currently - create" % (dn)
        print "-------------- Creating  ---------------"
        newObj= createMO(ucs, classid, dn, desired_settings)
        ucs.add_mo(newObj, modify_present=True)
    else:
        print "Object with Dn <%s> does exist - modify" %(dn)
        print "-------------- modifying ---------------"
        modifyMO(ucs, obj, desired_settings)

def isNewMOExpected(classid):
    create_MO_allowed=False
    createAllowedClasses = ['Adaptor', 'Lsboot']
    for req_class in createAllowedClasses:
        if req_class in classid:
            create_MO_allowed = True
            break
    return create_MO_allowed

def main(args):
    ucsclassload = yaml.load_all(args.config, yaml.RoundTripLoader)

    try:
        ucs = ImcHandle(args.ip, args.username, args.password)
        signal(SIGALRM, cimc_login_timeout_handler)
        alarm(1000)
        ucs.login(auto_refresh=True)
    except Exception as e:
        print e
        exit(1)

    if args.modify == 'True':
        print "-------------- Making change ---------------"
    elif args.modify == 'False':
        print "---------------- No change -----------------"

    try:
      for docs in ucsclassload:
        for classid, objects in docs.iteritems():
          for dn, csettings in objects.iteritems():
            #print "classid: %s, dn:%s"%(classid, dn)
            #print csettings

            if args.modify == 'True':
                if isNewMOExpected(classid):
                    modifyOrCreateMO(ucs, classid, dn, csettings)
                else:
                    modifyExistingMO(ucs, dn, csettings)
            elif args.modify == 'False':
                checkExistingMO(ucs, dn, csettings)
    except Exception as e:
      print e
      if ucs:
          ucs.logout()
          exit(1)

    ucs.logout()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Configure yaml policies for CIMC')
    parser.add_argument('-b', '--cfg', dest='config', required=True,
                        type=argparse.FileType('r'),
                        help='YAML config file for specific cimc setting')
    parser.add_argument('-i', '--ip', dest='ip', required=True,
                        help='IP of the target CIMC')
    parser.add_argument('-u', '--username', dest='username', default='admin', required=True,
                        help='Username for login')
    parser.add_argument('-p', '--password', dest='password', required=True,
                        help='Password for the user')
    parser.add_argument('-c', '--reallymakechange', dest='modify', default='False',
                        help='action to modify True/False')
    args = parser.parse_args()
    main(args)
