#! /usr/bin/python
'''
the script works on both CIMC 2.0(9f) and 3.0(3a)
It will audit check all network ports status
eg: python check_interface.py -i ip -u admin -p xxxx
'''
from imcsdk import *
from imcsdk.imchandle import ImcHandle
import argparse

def checkServerModel(handle):
    mgmtIf = handle.query_dn('sys/rack-unit-1/mgmt/if-1')
    print ('Hostname: %s ' % mgmtIf.hostname),
    rack = handle.query_dn("sys/rack-unit-1")
    print "Power:%s  Model:%s  SN:%s" %(rack.oper_power, rack.model, rack.serial)

def checkVICMediaTestFailurePorts(handle):
    vics = handle.query_classid("adaptorUnit")
    if len(vics)== 0:
        print "No vic card found"
    else:
        for vic in vics:
            print "<%s> Model: %s  Serial: %s"%(vic.dn, vic.model, vic.serial)
        vicports = handle.query_classid("adaptorExtEthIf")
        if len(vicports) == 0:
            print "No VIC ports found"
        else:
            for vicport in vicports:
                print "<%s> AdminSpeed: %s  OperSpeed: %s  Mac: %s  LinkState: %s" %(vicport.dn, vicport.admin_speed, vicport.oper_speed, vicport.mac, vicport.link_state)

def checkVNICProperty(handle):
    vnics = handle.query_classid("adaptorHostEthIf")
    infos = handle.query_classid("adaptorEthGenProfile")
    if len(vnics) == 0:
        print "No VNIC found"
    else:
        for vnic, info in zip(vnics, infos):
            print "<%s> UplinkPort: %s  Mac: %s  Pxeboot: %s  Vlan: %s  VlanMode: %s" %(vnic.dn, vnic.uplink_port, vnic.mac, vnic.pxe_boot, info.vlan, info.vlan_mode)

# this function will be later tested if broadcom card details could be queried in Imc 3.0
def checkBroadcomMediaTestFailurePorts(handle):
    nets = handle.query_classid("networkAdapterUnit")
    if len(nets) == 0:
        print "No more enabled network adaptors found"
        return
    else:
        for net in nets:
            print "<%s> slot: <%s> model: <%s>" %(net.dn, net.slot, net.model)

    netifs = handle.query_classid("networkAdapterEthIf")
    if len(netifs) == 0:
        print "No networkExtEthIfs found"
    else:
        for netif in netifs:
            print "<%s> ID: <%s> Mac: <%s> status: <%s>" %(netif.dn, netif.id, netif.mac, netif.status)

def main(args):
    handle = ImcHandle(args.host,args.username,args.password)
    try:
        handle.login()
    except Exception, err:
        print "Exception:", str(err)
        exit(1)
    checkServerModel(handle)
    checkVICMediaTestFailurePorts(handle)
    checkVNICProperty(handle)
    checkBroadcomMediaTestFailurePorts(handle)
    handle.logout()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='to check the server info, model, sn, cards')
    parser.add_argument('-i', '--host',dest="host", required=True,
                      help="[Mandatory] IMC IP Address")
    parser.add_argument('-u', '--username',dest="username", required=True,
                      help="[Mandatory] Account Username for IMC Login")
    parser.add_argument('-p', '--password',dest="password", required=True,
                      help="[Mandatory] Account Password for IMC Login")
    args = parser.parse_args()
    main(args)
